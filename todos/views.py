from django.shortcuts import render, redirect

from todos.models import TodoItem, TodoList
    #from todos.forms import TodoItemForm, TodoListForm

# Create your views here.

def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "lists": lists,
    }
    return render(request, "todos/list.html", context)
